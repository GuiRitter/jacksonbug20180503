package com.example.demo;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.notNullValue;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DemoApplicationTests {

	@LocalServerPort
	private int port;

	@Test
	public void contextLoads() {
	}

	@Test
	@SuppressWarnings("unchecked")
	public void testGet() {
		Map<Object, Object> map = get("http://localhost:" + port).body().jsonPath().getMap("$");
		map = (Map<Object, Object>) map.get("_links");
		map = (Map<Object, Object>) map.get("sale");
		System.out.println(getClass().getSimpleName() + " testGet " + map.get("href"));
	}

	@Test
	public void testNullList() {
		get("http://localhost:" + port + "/sales/1").then().body("saleItemList", notNullValue());
	}

	@Test
	public void testNullCompany() {
		get("http://localhost:" + port + "/sales/1").then().body("company", notNullValue());
	}

	@SuppressWarnings("unchecked")
	public void testPatch(String suffixURL) {
		Map<Object, Object> sale = get("http://localhost:" + port + "/sales/1").body().jsonPath().getMap("$");
		List<Map<Object, Object>> saleItemList = (List<Map<Object, Object>>) sale.get("saleItemList");
		for (Map<Object, Object> saleItem : saleItemList) {
			saleItem.put("amount", 2l * ((Number) saleItem.get("amount")).longValue());
		}
		given().body(sale).with().contentType("application/json")
				.then().expect().statusCode(200)
				.when().patch("http://localhost:" + port + suffixURL);
	}

	@Test
	public void testPatchWithout() {
		testPatch("/sales/1");
	}

	@Test
	public void testPatchWith() {
		testPatch("/sales/1?projection=sale");
	}
}
