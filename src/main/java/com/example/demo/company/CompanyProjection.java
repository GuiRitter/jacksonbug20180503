package com.example.demo.company;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "company", types = { Company.class })
public interface CompanyProjection {

	String getCompanyName();

	String getTradingName();
}
