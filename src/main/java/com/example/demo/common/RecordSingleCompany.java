package com.example.demo.common;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.example.demo.company.Company;

@MappedSuperclass
public abstract class RecordSingleCompany extends Record {

    @ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE }, optional= false)
    @JoinColumn(name="id_company")
    protected Company company;

    public RecordSingleCompany() {}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
}
