package com.example.demo.common;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.example.demo.company.Company;

@MappedSuperclass
public class RegistrySingleCompany extends Registry {

    @ManyToOne(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE }, optional= false)
    @JoinColumn(name="id_company")
    protected Company company;

    public RegistrySingleCompany() {}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
}
