package com.example.demo.common;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.example.demo.company.Company;

@MappedSuperclass
public class RegistryMultiCompany extends Registry {

	@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE })
	@NotNull
	@NotEmpty
	protected Set<Company> companySet = new HashSet<Company>();

	public RegistryMultiCompany() {}

	public Set<Company> getCompanySet() {

		return Collections.unmodifiableSet(this.companySet);
	}

	public void addCompany(Company company) {

		if (company == null)
			return;

		this.companySet.add(company);
	}

	public void setCompanySet(Set<Company> companySet) {
		this.companySet = companySet;
	}
}
