package com.example.demo.common;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotBlank;

@MappedSuperclass
public abstract class Registry extends Record {

//    @NotBlank
//    @Column(name = "code", length = 15)
//    protected String code;

    @NotBlank
    @Column(name = "name", length = 40)
    protected String name;

    public Registry() {}

//	public String getCode() {
//		return code;
//	}
//
//	public void setCode(String code) {
//		this.code = code;
//	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
