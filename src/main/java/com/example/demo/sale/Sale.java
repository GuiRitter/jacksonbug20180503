package com.example.demo.sale;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.example.demo.common.RegistrySingleCompany;
import com.example.demo.sale.item.SaleItem;

@Entity
@Table(name="sale")
@AttributeOverrides( { @AttributeOverride(name = "id", column = @Column(name = "id_sale")) } )
//@SaleConstraint
public class Sale extends RegistrySingleCompany {

//	@JsonManagedReference
	@OneToMany(cascade = { CascadeType.MERGE }, fetch = FetchType.EAGER, mappedBy="sale")
	private List<SaleItem> saleItemList = new ArrayList<SaleItem>();

	public Sale() {}

	public List<SaleItem> getSaleItemList() {
		return saleItemList;
	}

	public void setSaleItemList(List<SaleItem> saleItemList) {
		this.saleItemList = saleItemList;
	}
}
