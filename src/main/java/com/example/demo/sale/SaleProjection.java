package com.example.demo.sale;

import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.example.demo.company.CompanyProjection;
import com.example.demo.sale.item.SaleItemProjection;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Projection(name = "sale", types = { Sale.class })
public interface SaleProjection {

	CompanyProjection getCompany();

	String getName();

	List<SaleItemProjection_> getSaleItemList();

	public interface SaleItemProjection_ extends SaleItemProjection {

		@Override
		@JsonIgnore
		SaleProjection_ getSale();
	}
}
