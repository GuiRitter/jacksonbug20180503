package com.example.demo.sale;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SaleValidator implements ConstraintValidator<SaleConstraint, Sale> {

	@Override
	public boolean isValid(Sale value, ConstraintValidatorContext context) {

		System.err.println(getClass().getSimpleName() + " isValid");
		return value.getCompany() != null;
	}
}
