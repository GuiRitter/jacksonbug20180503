package com.example.demo.sale;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "sale", path = "sales", excerptProjection = SaleProjection.class)
public interface SaleRepository extends PagingAndSortingRepository<Sale, Long> {}
