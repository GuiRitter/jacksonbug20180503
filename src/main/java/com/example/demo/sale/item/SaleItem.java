package com.example.demo.sale.item;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.example.demo.common.RecordSingleCompany;
import com.example.demo.sale.Sale;

@Entity
@Table(name="sale_item")
@AttributeOverrides( { @AttributeOverride(name = "id", column = @Column(name = "id_sale_item")) } )
public class SaleItem extends RecordSingleCompany {

	private Integer amount;

	@ManyToOne
	@JoinColumn(name="id_sale")
	private Sale sale;

	public SaleItem() {}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Sale getSale() {
		return sale;
	}

	public void setSale(Sale sale) {
		this.sale = sale;
	}
}
