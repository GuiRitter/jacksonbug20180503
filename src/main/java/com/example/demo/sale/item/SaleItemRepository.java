package com.example.demo.sale.item;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "saleItem", path = "saleItems", excerptProjection = SaleItemProjection.class)
public interface SaleItemRepository extends PagingAndSortingRepository<SaleItem, Long> {}
