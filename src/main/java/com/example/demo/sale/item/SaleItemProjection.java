package com.example.demo.sale.item;

import java.util.List;

import org.springframework.data.rest.core.config.Projection;

import com.example.demo.company.CompanyProjection;
import com.example.demo.sale.SaleProjection;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Projection(name = "saleItem", types = { SaleItem.class })
public interface SaleItemProjection {

	CompanyProjection getCompany();

	Integer getAmount();

	SaleProjection_ getSale();

	public interface SaleProjection_ extends SaleProjection {

		@Override
		@JsonIgnore
		List<SaleItemProjection_> getSaleItemList();
	}
}
