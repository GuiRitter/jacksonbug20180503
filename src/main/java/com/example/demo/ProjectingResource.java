package com.example.demo;

import org.springframework.hateoas.Resource;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Projecting resource used for {@link ProjectingProcessor}. Does not include empty links in JSON, otherwise two
 * _links keys are present in returning JSON.
 * <br><br>
 * <a href="https://stackoverflow.com/a/40303490/1781376">Spring Data REST: projection representation of single resource</a>
 * @param <T>
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
class ProjectingResource<T> extends Resource<T> {

    ProjectingResource(final T content) {
        super(content);
    }
}
