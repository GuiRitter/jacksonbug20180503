package com.example.demo;

import java.io.BufferedReader;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@Component
public class DefaultHandlerInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		BufferedReader reader = request.getReader();
		String line;
		StringBuilder builder = new StringBuilder();
		while ((line = reader.readLine()) != null) {
			builder.append(line);
		}
		if (!builder.toString().trim().isEmpty()) {
			ObjectMapper mapper = new ObjectMapper();
			// to enable standard indentation ("pretty-printing"):
			mapper.enable(SerializationFeature.INDENT_OUTPUT);
			Map<?, ?> map = mapper.readValue(builder.toString(), Map.class);
			builder.setLength(0);
			builder.append(mapper.writeValueAsString(map));
		}
		System.err.println(getClass().getSimpleName() + " preHandle " + builder);

		return true;
	}
}
