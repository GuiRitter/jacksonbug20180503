/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.example.demo.company.Company;
import com.example.demo.company.CompanyRepository;
import com.example.demo.sale.Sale;
import com.example.demo.sale.SaleRepository;
import com.example.demo.sale.item.SaleItem;
import com.example.demo.sale.item.SaleItemRepository;

@Component
public class DatabaseLoader implements CommandLineRunner {

	private final CompanyRepository companyRepository;

	private final SaleRepository saleRepository;

	private final SaleItemRepository saleItemRepository;

	@Autowired
	public DatabaseLoader(
			CompanyRepository companyRepository,
			SaleRepository saleRepository,
			SaleItemRepository saleItemRepository) {
		this.companyRepository = companyRepository;
		this.saleRepository = saleRepository;
		this.saleItemRepository = saleItemRepository;
	}

	@Override
	public void run(String... strings) throws Exception {
		Company company = new Company();
		company.setCompanyName("Acme Corporation");
		company.setTradingName("ACME");
		company = companyRepository.save(company);

		List<SaleItem> list = new ArrayList<>();

		Sale sale = new Sale();
		sale.setCompany(company);
		sale.setName("Sale 01");
		sale.setSaleItemList(list);
		sale = saleRepository.save(sale);

		SaleItem item = new SaleItem();
		item.setCompany(company);
		item.setSale(sale);
		item.setAmount(3);
		item = saleItemRepository.save(item);
		list.add(item);

		item = new SaleItem();
		item.setCompany(company);
		item.setSale(sale);
		item.setAmount(5);
		item = saleItemRepository.save(item);
		list.add(item);

		saleRepository.save(sale);
	}
}
